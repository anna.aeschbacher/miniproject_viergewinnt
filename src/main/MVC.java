package main;

import controller.Controller;
import javafx.application.Application;
import javafx.stage.Stage;
import model.Model;
import view.View;

public class MVC extends Application{
	private Model model;
	private View view;
	private Controller controller;

	public static void main(String[] args) {
		launch();

	}
	
	public void start(Stage stage) throws Exception{
		this.model = new Model();
		this.view = new View(stage, model);
		this.controller = new Controller (model, view);
		view.start();
		
	}

}
