package view;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;
import model.Model;

public class View {
	private Stage stage;
	private Model model;

	protected MenuBar menus = new MenuBar();
	protected Menu menuFile = new Menu("Datei");
	protected MenuItem quit = new MenuItem("Beenden");

	protected Button newGame = new Button("Neues Spiel");
	protected Button enlarge = new Button("Spielfeld vergrössern");
	public Button[][] buttons; // initialized in "new game" method

	protected GridPane gameGrid = new GridPane();
	protected HBox hbox = new HBox();

	public View(Stage stage, Model model) {
		this.stage = stage;
		this.model = model;

		BorderPane root = new BorderPane();

		menus.getMenus().add(menuFile);
		menuFile.getItems().add(quit);
		root.setTop(menus);

		hbox.getChildren().addAll(enlarge, newGame);
		root.setBottom(hbox);

		root.setCenter(gameGrid);

		Scene scene = new Scene(root);
		stage.setTitle(" Vier gewinnt");
		// scene.getStylesheets().add(getClass().getResouce("four.css").toExternalForm())
		stage.setScene(scene);
		stage.setResizable(true);
		
		gameGrid.setAlignment(Pos.CENTER);
		gameGrid.getColumnConstraints().addAll(new ColumnConstraints(100,100,Double.MAX_VALUE), 
	            new ColumnConstraints(100,100,Double.MAX_VALUE),
	            new ColumnConstraints(100,100,Double.MAX_VALUE),
	            new ColumnConstraints(100,100,Double.MAX_VALUE));
		gameGrid.getRowConstraints().addAll(new RowConstraints(100,100,Double.MAX_VALUE), 
	            new RowConstraints(100,100,Double.MAX_VALUE),
	            new RowConstraints(100,100,Double.MAX_VALUE),
	            new RowConstraints(100,100,Double.MAX_VALUE));
	    
	}
	
	public void start() {
		stage.show();
	}

	protected void newGame(int width, int height) {
		gameGrid.getChildren().clear();
		buttons = new Button[height][width];
		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				Button btn = new Button();
				buttons[row][col] = btn;
				gameGrid.add(btn, col, row);
			}

		}
	}
	
	protected void showMessage() {
		Alert alert = new Alert(AlertType.NONE);
		alert.setHeaderText("Spieler x hat gewonnen!");
		alert.showAndWait();
	}

}
